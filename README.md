# Extract thermal images

![Shows the three images and themp data extracted from thermal camera](img/example.png "Outputs examples")

The TOOLTOP ET692C thermal imaging camera stores jpg's with extra information.

This project tries to extract all info's.


## Use

```bash
python3 et692-decoder.py 20240608-230452.jpg
```

This will generate a `output` directory with the following files:
```
output/20240608-230452_header.json # date/time; hot spot, cold spot, center temp, all temps, ...
output/20240608-230452_thermal.png # Only the thermal image
output/20240608-230452_screen.jpg # Only the screen shot
output/20240608-230452_visual.jpg # Only the visual image
output/20240608-230452_temps.csv # Temperature's as csv
```
