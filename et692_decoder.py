import struct
import itertools
from pathlib import Path

from PIL import Image

thermal_res = [192, 256]
JPEG_HEADER = bytearray.fromhex("ffd8ffe000104a464946")


def decode_tail(bytes):
    tail_start = -4 * 12
    data = struct.unpack("HHIHHIHHIIIIIII", bytes[tail_start:])
    tail_stop = tail_start
    center = {
        "x": data[0],
        "y": data[1],
        "temp": data[2] / 10,
    }
    hot = {
        "x": data[3],
        "y": data[4],
        "temp": data[5] / 10,
    }
    cold = {
        "x": data[6],
        "y": data[7],
        "temp": data[8] / 10,
    }
    image_mix = data[12]
    e = data[9]

    something = struct.unpack("I", bytes[(-8 * 7 * 2) : (-8 * 7 * 2) + 4])
    unknown = [data[10], data[11], data[13], data[14], something]

    tail_start -= 20
    date = bytes[tail_start:tail_stop].split(b"\x00", 1)[0].decode("utf-8")
    tail_stop = tail_start

    tail_start -= 20
    version = bytes[tail_start:tail_stop].split(b"\x00", 1)[0].decode("utf-8")
    tail_stop = tail_start

    tail_start -= 20
    model = bytes[tail_start:tail_stop].split(b"\x00", 1)[0].decode("utf-8")
    tail_stop = tail_start
    parsed = {
        "center": center,
        "hot": hot,
        "cold": cold,
        "image_mix_ratio": image_mix,
        "e": e,
        "unknown": unknown,
        "date": date,
        "version": version,
        "model": model,
    }
    return parsed


def get_image(bytes):
    stop = -(4 * 12) - (20 * 3) - 4
    start = stop - (thermal_res[0] * thermal_res[1])

    image = bytes[start:stop]

    return image


def get_temps(bytes):
    stop = -(4 * 12) - (20 * 3) - (thermal_res[0] * thermal_res[1]) - 4
    start = stop - (thermal_res[0] * thermal_res[1] * 2)

    temps = bytes[start:stop]
    temps = itertools.batched(temps, 2)
    temps = [float(low | (high << 8)) / 10 for low, high in temps]
    temps = itertools.batched(temps, thermal_res[0])

    return temps


def find_jpegs(bytes):
    stop = -(4 * 12) - (20 * 3) - (thermal_res[0] * thermal_res[1]) - 4
    stop = stop - (thermal_res[0] * thermal_res[1] * 2)
    start_visual_image = bytes.find(JPEG_HEADER, 1)

    screen_shot_data = bytes[0:start_visual_image]
    visual_image_data = bytes[start_visual_image:stop]

    return screen_shot_data, visual_image_data


if __name__ == "__main__":
    import sys
    import json

    out_dir = Path("output")
    out_dir.mkdir(exist_ok=True)
    out_files = str(out_dir / Path(sys.argv[1]).stem)

    data = open(sys.argv[1], "rb").read()
    header = decode_tail(data)
    header_json = json.dumps(header, indent=2)
    print(header_json)

    img_data = get_image(data)
    thermal_img = Image.frombytes("L", thermal_res, img_data)
    thermal_img.save(out_files + "_thermal.png")

    temps = list(get_temps(data))
    with open(out_files + "_temps.csv", "w") as csv:
        for line in temps:
            line = [str(i) for i in line]
            csv.write(", ".join(line) + "\n")

    screen, visual = find_jpegs(data)
    with open(out_files + "_screen.jpg", "wb") as fh:
        fh.write(screen)

    with open(out_files + "_visual.jpg", "wb") as fh:
        fh.write(visual)

    header["temps"] = temps
    with open(out_files + "_header.json", "w") as fh:
        fh.write(json.dumps(header, indent=2))
